const next = document.getElementsByClassName("left")[0];
const prev = document.getElementsByClassName("right")[0];

let n = 1;

next.addEventListener("click", function() {
  fetch(`http://localhost:8080/pagination?page=${n + 1}`)
    .then(res => {
      return res.json();
    })
    .then(data => {
      const { allImages, pagination } = data;
      if (n === pagination.total) {
        return;
      }
      console.log(n);
      n++;
      const imgFormServer = document.getElementsByClassName(
        "img_form_server"
      )[0];

      imgFormServer.src = `${allImages}`;
    })
    .catch(err => {
      console.log(err);
    });
});

prev.addEventListener("click", function() {
  fetch(`http://localhost:8080/pagination?page=${n - 1}`)
    .then(res => {
      return res.json();
    })
    .then(data => {
      const { allImages } = data;
      if (!allImages) {
        return;
      }
      n--;
      const imgFormServer = document.getElementsByClassName(
        "img_form_server"
      )[0];

      imgFormServer.src = `${allImages}`;
    })
    .catch(err => {
      console.log(err);
    });
});

const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const { img } = require("./pagination");
const morgan = require("morgan");
const port = 8080;
server = express();
const static = path.join(__dirname, "public");
const views = path.join(__dirname, "views");

server.use(
  bodyParser.urlencoded({
    extended: false
  })
);

server.use(bodyParser.json());
server.use(morgan("tiny"));
server.set("view engine", "ejs");
server.use(express.static(static));
server.set("views", views);

//@get Root Route
//@get Index About Contact Routes

const allImages = img.map(items => {
  return items;
});

server.get("/", (req, res, next) => {
  res.render("index", { allImages: allImages[0] });
});

//@get more_info
server.get("/pagination", (req, res, next) => {
  const page = req.query.page;
  const limit = 1;
  const skip = parseInt((page - 1) * limit);

  res.json({
    allImages: allImages[skip],
    pagination: {
      total: allImages.length
    }
  });
});

server.listen(port, () => console.log(`Server works on port:${port}`));
